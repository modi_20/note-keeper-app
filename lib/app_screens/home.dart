import 'package:flutter/material.dart';

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
       child: Container(
          alignment: Alignment.center,
          color: Colors.blueGrey,
          padding: EdgeInsets.only(left: 10.0, top: 40.0),
          child: Column(
            children: <Widget>[
               Row(
                 children: <Widget>[
                   Expanded(child: Text(
                     "Welcome",
                     textDirection: TextDirection.ltr,
                     style: TextStyle(
                         decoration: TextDecoration.none,
                         fontFamily: 'Lobster',
                         fontSize: 35.0,
                         color: Colors.greenAccent
                     ),
                   )),
                   Expanded(child: Text(
                     "Work...!!!",
                     textDirection: TextDirection.ltr,
                     style: TextStyle(
                         decoration: TextDecoration.none,
                         fontFamily: 'Lobster',
                         fontSize: 35.0,
                         color: Colors.greenAccent
                     ),
                   )),
                 ],
               ),
               Row(
                 children: <Widget>[
                   Expanded(child:ImageAss())
                 ],
               ),
               Row(
                 children: <Widget>[
                   Expanded(child:Button())
                 ],
               )
             ],
          )
        ),
    );
  }
}

class ImageAss extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    AssetImage assetImage = AssetImage('images/lion-unsplash.jpg');
    Image image = Image(image: assetImage,width: 250.0,height: 200.0);
    return Container(child: image,);
  }
}

class Button extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 30.0),
      child: RaisedButton(
          color: Colors.deepOrangeAccent,
          child: Text(
            "Book Flight!",
            style: TextStyle(
              fontSize: 20.0,
              fontFamily: 'Lobster',
               color: Colors.white
            ),
          ),
          elevation: 10.0,
          onPressed: (){
              book(context);
      }),
    );
  }

  void book(BuildContext context) {

    var alertDialog = AlertDialog(
      title: Text("Flight Booked Successfully"),
      content: Text("Have a pleasant flight!"),
    );

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alertDialog;
      }
    );
  }
}